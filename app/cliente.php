<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cliente extends Model
{
    protected $fillable = [
       'nombre' ,
       'apellidos',
       'cedula' ,
       'direccion',
       'telefono',
       'fecha_nacimiento',
       'email'
    ];
}

     
    
