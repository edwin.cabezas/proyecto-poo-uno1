@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Crear un Clientes</div>
                <div class="col text-right">
                    <a href="{{route('list.clientes')}}" class="btn-sm btn-succes">Cancelar</a>
                </div>
                <div class="card-body">
                    <form role="form" method="post" action="{{route('guardar.clientes')}}">
                        {{csrf_field()}}
                        {{method_field('post')}}
                    <div class="row">
                    <div class="col-lg-4">
                        <label class="from-control-label" fro="nombre">Nombre del producto</label>
                        <input type="text" class="from-control" name="nombre">
                </div>

                <div class="col-lg-4">
                        <label class="from-control-label" fro="apellidos">Apellidos</label>
                        <input type="text" class="from-control" name="apellidos">
                </div>

                <div class="col-lg-4">
                        <label class="from-control-label" fro="cedula">Cedula</label>
                        <input type="number" class="from-control" name="cedula">
                </div>

                <div class="col-lg-4">
                        <label class="from-control-label" fro="direccion">Direccion</label>
                        <input type="text" class="from-control" name="direccion">
                </div>

                <div class="col-lg-4">
                        <label class="from-control-label" fro="telefono">Telefono</label>
                        <input type="number" class="from-control" name="telefono">
                </div>

                <div class="col-lg-4">
                        <label class="from-control-label" fro="fecha_nacimiento">Fecha de nacimiento</label>
                        <input type="text" class="from-control" name="fecha_nacimiento">
                </div>

                <div class="col-lg-4">
                        <label class="from-control-label" fro="email">Email</label>
                        <input type="text" class="from-control" name="email">
                </div>
                </div>

                <button type="submit" class="btn btn-success pull-right">Guardar</button>
                </form>

              </div>
            </div>
        </div>
    </div>
</div>
@endsection